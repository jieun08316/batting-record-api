package com.example.bettingrecordapi.entity;


import jakarta.persistence.*;
import jakarta.persistence.criteria.Fetch;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class CafeRecord {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "peopleId",nullable = false)
    private People people;

    @Column(nullable = false)
    private LocalDate dateCafe;

    @Column(nullable = false)
    private Double price;


}
