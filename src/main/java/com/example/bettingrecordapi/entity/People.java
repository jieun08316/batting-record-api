package com.example.bettingrecordapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.query.sql.internal.ParameterRecognizerImpl;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class People {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private short age;

    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false, unique = true, length = 13)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

}
