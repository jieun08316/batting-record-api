package com.example.bettingrecordapi.model.people;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleItem {
    private long id;

    private String name;

    private short age;

    private String phoneNumber;

}
