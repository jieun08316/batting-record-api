package com.example.bettingrecordapi.model.people;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PeopleCreatRequest {
    private String name;

    private short age;

    private LocalDate birthDay;

    private String phoneNumber;

    private String etcMemo;


}
