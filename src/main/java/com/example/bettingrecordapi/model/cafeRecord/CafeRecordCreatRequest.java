package com.example.bettingrecordapi.model.cafeRecord;


import com.example.bettingrecordapi.entity.People;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CafeRecordCreatRequest {

    private LocalDate dateCafe;
    private Double price;

}
