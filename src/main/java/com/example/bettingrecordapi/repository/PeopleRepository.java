package com.example.bettingrecordapi.repository;

import com.example.bettingrecordapi.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
