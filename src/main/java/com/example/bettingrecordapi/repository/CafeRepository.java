package com.example.bettingrecordapi.repository;


import com.example.bettingrecordapi.entity.CafeRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CafeRepository extends JpaRepository<CafeRecord, Long> {
}
