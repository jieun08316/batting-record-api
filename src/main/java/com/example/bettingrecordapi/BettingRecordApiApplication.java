package com.example.bettingrecordapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BettingRecordApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BettingRecordApiApplication.class, args);
    }

}
