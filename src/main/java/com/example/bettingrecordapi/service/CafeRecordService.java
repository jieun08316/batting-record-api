package com.example.bettingrecordapi.service;

import com.example.bettingrecordapi.entity.CafeRecord;
import com.example.bettingrecordapi.entity.People;
import com.example.bettingrecordapi.model.cafeRecord.CafeRecordCreatRequest;
import com.example.bettingrecordapi.model.cafeRecord.CafeRecordItem;
import com.example.bettingrecordapi.repository.CafeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CafeRecordService {

    private final CafeRepository cafeRepository;

    public void setCafeRecord(People people, CafeRecordCreatRequest request) {

        CafeRecord addData = new CafeRecord();
        addData.setPeople(people);
        addData.setDateCafe(request.getDateCafe());
        addData.setPrice(request.getPrice());
        cafeRepository.save(addData);
    }

    public List<CafeRecordItem> getCafeRecords() {

        List<CafeRecord> originList = cafeRepository.findAll();
        List<CafeRecordItem> result = new LinkedList<>();

        for (CafeRecord cafeRecord : originList) {
            CafeRecordItem addItem = new CafeRecordItem();
            addItem.setId(cafeRecord.getPeople().getId());
            addItem.setId(cafeRecord.getId());
            addItem.setPeople(cafeRecord.getPeople());
            addItem.setDateCafe(cafeRecord.getDateCafe());
            result.add(addItem);
        }
        return result;
    }


}

