package com.example.bettingrecordapi.service;

import com.example.bettingrecordapi.entity.People;
import com.example.bettingrecordapi.model.people.PeopleCreatRequest;
import com.example.bettingrecordapi.model.people.PeopleItem;
import com.example.bettingrecordapi.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public People getData(long id) {
        return peopleRepository.findById(id).orElseThrow();
    }

    public void setPeople(PeopleCreatRequest request) {

        People addData = new People();
        addData.setName(request.getName());
        addData.setAge(request.getAge());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        peopleRepository.save(addData);

    }


    public List<PeopleItem> getPeoples() {
        List<People> originList = peopleRepository.findAll();

        List<PeopleItem> result = new LinkedList<>();

        for (People people : originList) {
            PeopleItem addItem = new PeopleItem();
            addItem.setId(people.getId());
            addItem.setName(people.getName());
            addItem.setAge(people.getAge());
            addItem.setPhoneNumber(people.getPhoneNumber());

            result.add(addItem);
        }
        return result;

    }

}
