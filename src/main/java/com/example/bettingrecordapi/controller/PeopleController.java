package com.example.bettingrecordapi.controller;

import com.example.bettingrecordapi.model.people.PeopleCreatRequest;
import com.example.bettingrecordapi.model.people.PeopleItem;
import com.example.bettingrecordapi.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/people")
public class PeopleController {

    private final PeopleService peopleService;
    @PostMapping("/list")
    public String setPeople(@RequestBody PeopleCreatRequest request){
        peopleService.setPeople(request);
        return "OK";
    }
    @GetMapping("/all")
    public List<PeopleItem> getPeoples(){
       return peopleService.getPeoples();

    }


}
