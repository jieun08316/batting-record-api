package com.example.bettingrecordapi.controller;

import com.example.bettingrecordapi.entity.People;
import com.example.bettingrecordapi.model.cafeRecord.CafeRecordCreatRequest;
import com.example.bettingrecordapi.model.cafeRecord.CafeRecordItem;
import com.example.bettingrecordapi.model.people.PeopleItem;
import com.example.bettingrecordapi.service.CafeRecordService;
import com.example.bettingrecordapi.service.PeopleService;
import jakarta.persistence.Id;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/cafe-record")
public class CafeRecordController {

    private final CafeRecordService cafeRecordService;
    private final PeopleService peopleService;
    @PostMapping("/new/people-id/{peopleId}")
    public String setCafeRecord(@PathVariable long peopleId ,@RequestBody CafeRecordCreatRequest request){
         People people = peopleService.getData(peopleId);
        cafeRecordService.setCafeRecord(people,request);
    return "OK";
    }
    @GetMapping("/all")
    public List<CafeRecordItem> getCafeRecords(){
        return cafeRecordService.getCafeRecords();

    }
}


